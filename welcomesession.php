<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>home</title>
    <link href="homesession.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="container">
    <img src="logo.jpg" alt="ashley logo" class="logo">
</div>

<div class="nav">
    <ul>
        <li><a href="welcomesession.php">Home</a></li>

        <div class="bots">
            <div class="btn"><li><a href="#">Arguments</a></li></div>
            <div class="dropdown">
                <a href="#">Configuration</a>
                <a href="#">Link 2</a>
                <a href="#">Link 3</a>
            </div>
        </div>

        <li><a href="#">about me</a></li>
        <li><a href="#">search</a></li>
        <li><a href="logout.php">log-out</a></li>


    </ul>
</div>

<?php
session_start();
if (isset($_SESSION['user'])) {
    echo "<div class='w-page'> ";
    echo "Welcome " . $_SESSION['user'];
    echo "</div>";
}
else
{
    echo "<script>location.href='logsession.php'</script>";
}
?>

<h1>Informatics</h1>

<div class="slide-container">

    <input type="radio" name="images" id="i1" checked>
    <input type="radio" name="images" id="i2">
    <input type="radio" name="images" id="i3">


    <div class="slide-image" id="one">
        <img src="raspberry.jpg" alt="picture">

        <label for="i3" class="prev"></label>
        <label for="i2" class="next"></label>

    </div>


    <div class="slide-image" id="two">
        <img src="programming.jpg" alt="picture">

        <label for="i1" class="prev"></label>
        <label for="i3" class="next"></label>

    </div>


    <div class="slide-image" id="three">
        <img src="web.jpg" alt="picture">

        <label for="i2" class="prev"></label>
        <label for="i1" class="next"></label>

    </div>

    <div class="dot">
        <label class="dots" id="dots1" for="i1"></label>
        <label class="dots" id="dots2" for="i2"></label>
        <label class="dots" id="dots3" for="i3"></label>

    </div>
</div>

<h1>Projects to download or to configure</h1>


<div class="pink-container">
    <div class="container">
        <ul>

            <li>
                <figure>
                    <a href="raspconfig.php"><img src="raspberry.jpg" alt="rasp"  height="200" width="200" class="fade"></a>
                    <blockquote>learn to configure<br> raspberry pi</blockquote>
                </figure>
            </li>

            <li>
                <figure>
                    <a href="apache.php"><img src="apache.jpg" alt="apache"  height="200" width="200"></a>
                    <blockquote>  learn to configure<br> apache2</blockquote>

                </figure>
            </li>

        </ul>
    </div>
</div>



</body>
</html>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>home</title>
    <link href="homesession.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="container">
    <img src="logo.jpg" alt="ashley logo" class="logo">
</div>

<div class="nav">
    <ul>
        <li><a href="welcomesession.php">Home</a></li>

        <div class="bots">
            <div class="btn"><li><a href="#">Arguments</a></li></div>
            <div class="dropdown">
                <a href="#">Configuration</a>
                <a href="#">Link 2</a>
                <a href="#">Link 3</a>
            </div>
        </div>

        <li><a href="#">about me</a></li>
        <li><a href="#">search</a></li>
        <li><a href="logout.php">log-out</a></li>

    </ul>
</div>

<?php
session_start();

if (isset($_SESSION['user']))
{
    echo "<div class='w-page'> " ;
    echo "Welcome to raspberry configuration <br>". $_SESSION['user'];
    echo "</div>";
}else
{
    echo "<script>location.href='logsession.php'</script>";
}

?>



<div class="pink-container">


        <h1>How to configure Raspberry pi</h1>

        <h3 >What is RASPBERRY PI</h3>
            <img src="raspberry.jpg" width="150" height="150"> </a>
        <p> is a low cost, credit-card sized computer that plugs into a computer monitor or TV,<br>
            and uses a standard keyboard and mouse.<br>
            It is a capable little device that enables people of all ages to explore computing,<br>
            and to learn how to program in languages like Scratch and Python.<br>
            It’s capable of doing everything you’d expect a desktop computer to do,<br>
            from browsing the internet and playing high-definition video,<br>
            to making spreadsheets, word-processing, and playing games.<br>
            <br>
            <br>
            What’s more, the Raspberry Pi  has the ability to interact with the outside world,<br>
            and has been used in a wide array of digital maker projects,<br>
            from music machines and parent detectors to weather stations and tweeting birdhouses with infra-red cameras.<br>
            We want to see the Raspberry Pi being used by kids all over the world to learn to<br>
            program and understand how computers work.<br>
            <br>
            <br>
            was created with the goal of education in mind. This ultra-tiny computer was designed to be small and cheap<br>
            so that schools could easily afford them in order to teach students about computers in the classroom.<br>
            This is great for two reasons, the first is that it provides extremely cheap access to a computer,<br>
            and second it is a great tool for learning more about computers (student or not)!<br>
        </p>

        <br>

        <h3>Equipment List</h3>

        <br>

        <ul>
            <li>
                <figure>
                <a href="https://www.amazon.com/s?k=Raspberry+Pi">
                    <img src="rasp.jpg" width="70" height="70"></a>
                    <blockquote>Raspberry Pi 2, 3, or 4</blockquote>
                </figure>
            </li>

            <li>
                <figure>
                    <a href="https://www.amazon.com/Sandisk-Ultra-Micro-UHS-I-Adapter/dp/B073K14CVB">
                        <img src="sd.jpg" width="70" height="70"></a>
                    <blockquote> Micro SD Card</blockquote>
                </figure>
            </li>

            <li>
                <figure>
                    <a href="https://www.amazon.com/s?k=Raspberry+Pi+Power+Supply">
                        <img src="power.jpg" width="70" height="70"></a>
                    <blockquote> Power Supply</blockquote>
                </figure>
            </li>

            <li>
                <figure>
                    <a href="https://www.amazon.com/s?k=ethernet+cable">
                        <img src="cable.jpg" width="70" height="70"></a>
                    <blockquote> Ethernet Cord or WiFi dongle<br> (The Pi 3 and 4 has WiFi inbuilt)</blockquote>
                </figure>
            </li>

        </ul>

        <h3>Here’s how it works</h3>

        <p> An SD card inserted into the slot on the board acts as the hard drive for the Raspberry Pi.
            It is powered by USB and the video output can be hooked up to a traditional RCA TV set,
            a more modern monitor, or even a TV using the HDMI port.
            This gives you all of the basic abilities of a normal computer.
            It also has an extremely low power consumption of about 3 watts.
            To put this power consumption in perspective, you could run over 30 Raspberry Pi’s in place of a standard light bulb!</p>

        <h3>download the operating systems</h3>
        <h4>Download NOOBS</h4>
        <p>  Using NOOBS is the easiest way to install Raspbian on your SD card. To get hold of a copy of NOOBS
            <br>
            NOOBS stands for new out of the box software
        </p>
        <a href="https://www.raspberrypi.org/downloads/" ><img src="noods.jpg"width="300" height="300"></a>
        <p>
            The simplest option is to download the zip archive of the files
        </p>

        <img src="zip.jpg"width="400" height="300">

        <p>
            Extracting NOOBS from the zip archive<br>
            you will need to extract the files from the NOOBS zip archive you downloaded from the Raspberry Pi website.<br>
            Go to your Downloads folder and find the zip file you downloaded.<br>
            Extract the files and keep the resulting Explorer/Finder window open.<br>
            <br>
            Copying the files
            <br>
            Now open another Explorer/Finder window and navigate to the SD card. It’s best to position the two windows side by side.<br>
            Select all the files from the NOOBS folder and drag them onto the SD card.
        </p>

        <img src="copy.jpg"width="500" height="300"></a>

        <p>
            Eject the SD card.<br>
            Booting from NOOBS<br>
            Once the files have been copied over, insert the micro SD Card into your Raspberry Pi, and plug the Pi into a power source.<br>
            You will be offered a choice when the installer has loaded. You should check the box for Raspbian, and then click Install.
        </p>

        <img src="install.jpg"width="400" height="400"></a>

    <p>
        Click Yes at the warning dialog, and then sit back and relax. <br>
        It will take a while, but Raspbian will install.
    </p>
    <img src="pi.jpg"width="400" height="400"></a>

    <p>
        When Raspbian has been installed, click OK and your Raspberry Pi will restart and Raspbian will then boot up.
    </p>

    <img src="done.jpg"width="300" height="300"></a>

    <p>
        after rebooting it will ask you to choose your country
    </p>

    <img src="country.jpg"width="300" height="300"></a>

    <p>
        after choosing you country <br>
        it will ask you for a password.<br>
        by default password is raspberry
    </p>
    <img src="pass.jpg"width="300" height="300"></a>
    <p>
        after entering the password it will show a wifi network.
        most raspberry pi have their wifi built in.
        you can connect directly using ethernet as well
    </p>

    <img src="wrasp.JPG."width="300" height="300"></a>

    <p>
        after connecting to wifi it will ask you for updates,<br>
        simply click next and its automatically check for you
    </p>
    <img src="up.JPG."width="300" height="300"></a>

    <h1>CONGRATULATIONS DOWNLOAD COMPLETED </h1>
</div>

</body>
</html>
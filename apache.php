<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>home</title>
    <link href="homesession.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="container">
    <img src="logo.jpg" alt="ashley logo" class="logo">
</div>

<div class="nav">
    <ul>
        <li><a href="welcomesession.php">Home</a></li>

        <div class="bots">
            <div class="btn"><li><a href="#">Arguments</a></li></div>
            <div class="dropdown">
                <a href="#">Configuration</a>
                <a href="#">Link 2</a>
                <a href="#">Link 3</a>
            </div>
        </div>

        <li><a href="#">about me</a></li>
        <li><a href="#">search</a></li>
        <li><a href="logout.php">log-out</a></li>

    </ul>
</div>

<?php
session_start();

if (isset($_SESSION['user']))
{
    echo "<div class='w-page'> " ;
    echo "Welcome to apache2 configuration <br>". $_SESSION['user'];
    echo "</div>";
}else
{
    echo "<script>location.href='logsession.php'</script>";
}

?>
</body>
</html>
